# -*- encoding: utf-8 -*-
$:.push File.expand_path(File.join('..', 'lib'), __FILE__)

Gem::Specification.new do |s|
  s.name        = 'ebs_tagger'
  s.version     = File.read('VERSION').strip
  s.platform    = Gem::Platform::RUBY
  s.author      = 'Hunter Wong'
  s.email       = 'quishee@gmail.com'
  s.summary     = 'Tags EBS volumes'
  s.homepage    = 'https://bitbucket.org/oryhara/ebs_tagger/src'
  s.license     = 'MIT'
  s.description = 'Goes through all AWS instances in a region and tags their attached volumes with their own tags'

  s.add_runtime_dependency 'aws-sdk', '~> 2'

  s.files         = Dir['bin/*']
  s.executables   = %w[ ebs_tagger ]
end
