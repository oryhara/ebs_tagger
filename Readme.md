# EBS Tagger

This gem copies the tags from each AWS EC2 instance in specified regions to the EBS volumes attached to those instances. 

example:
Instance aws-instance-1 in region us-west-1 has the following tags:

    [
      {key: "Name", value: "aws-instance-1"},
      {key: "Environment", value: "Production"}
    ]

Running __ebs\_tagger__ will add these tags to the EBS volumes that are attached:

    i-1234567890abcdefg - aws-instance-1
    ----------
    1 volume(s)
    vol-09876543210zyxwvut
    "Name => aws-instance-1" added to volume
    "Environment => Production" added to volume
    us-west-1: Instance 1 of 1 done

***N.B.*** There is a limit of 10 tags per volume. 

## Options
### Required Options

Required options are:

* AWS Access key ID
* AWS Secret access key
* AWS Region(s)


    ebs_tagger -i aws_id -k aws_key -r aws-region-1,aws-region-2

### Other Options

The ```--add-id``` option will include the Instance ID in the tags to be synced to the volumes. 

    i-1234567890abcdefg - aws-instance-1
    ----------
    1 volume(s)
    vol-09876543210zyxwvut
    "Name => aws-instance-1" added to volume
    "Environment => Production" added to volume
    "Instance ID => i-1234567890abcdefg" added to volume
    us-west-1: Instance 1 of 1 done


The ```--only``` option will sync only the specified tags.

    $ ebs_tagger -i aws_id -k aws_key -r us-west-1 --only Name

    i-1234567890abcdefg - aws-instance-1
    ----------
    1 volume(s)
    vol-09876543210zyxwvut
    "Name => aws-instance-1" added to volume
    us-west-1: Instance 1 of 1 done


The ```--ignore``` option will NOT sync the specified tags.

    $ ebs_tagger -i aws_id -k aws_key -r us-west-1 --ignore Name

    i-1234567890abcdefg - aws-instance-1
    ----------
    1 volume(s)
    vol-09876543210zyxwvut
    Ignoring "Name"
    "Environment => Production" added to volume
    us-west-1: Instance 1 of 1 done



