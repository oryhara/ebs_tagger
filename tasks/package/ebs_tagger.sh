#!/bin/bash
set -e
EBS_TAGGER_RUBY="${EBS_TAGGER_RUBY:-/usr/bin/ruby2.2}"
EBS_TAGGER_ROOT="${EBS_TAGGER_ROOT:-/opt/ebs_tagger}"
export BUNDLE_GEMFILE="$EBS_TAGGER_ROOT/Gemfile"
unset BUNDLE_IGNORE_CONFIG
exec "$EBS_TAGGER_RUBY" -rbundler/setup "$EBS_TAGGER_ROOT/bin/ebs_tagger" $@