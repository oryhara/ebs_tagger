#!/usr/bin/env bash
set -x
set -e

ARTIFACTS="${1:-artifacts}"

ruby=ruby2.2
version="$(cat VERSION)-${BUILD_NUMBER:-1}+$(git rev-parse --short HEAD)"

rm -rf doc
rm -rf etc
rm -rf tmp
rm -rf .bundle
rm -rf .vagrant
rm -rf vendor
rm -rf pkg
mkdir -p pkg

bundle update
bundle package --all --all-platforms

bundle exec fpm -n ebs_tagger \
  --after-install tasks/package/post-install.sh \
  --after-upgrade tasks/package/post-install.sh \
  -d "$ruby" -d "$ruby-dev" \
  -s dir -t deb -v "$version" \
  tasks/package/ebs_tagger.sh=/usr/local/bin/ebs_tagger \
  ./=/opt/ebs_tagger

# gem uninstall -aIx
# dpkg -i *.deb
# ls -la /opt/ebs_tagger
# ebs_tagger --help

mkdir -p $ARTIFACTS
mv *.deb $ARTIFACTS